#include <string.h>
#include <sstream>
#include <ctime>

int max (int a, int b)
{
   if (a>b) return(a) ; else return (b);
}

int min (int a, int b)
{
   if (a>b) return(b); else return (a);
}

class Date
{
 public:
    int divided;
    void display();                   // function to display date
    int GetMonth();
    void SetMonth(int mn);
    void SetDay(int day);
    void SetYear(int year);
    int GetDay();
    int GetYear();
    Date operator/(int x);
    int DaysSoFar();
    
    // constructor definition
    Date ()
    {
         divided=0;
         month = day = year = 1;
    }
    
    Date(std::string dt)
    {
        int i=0,j=0;
        char days[2];
        char months[2];
        char years[4];
        for(i; dt[i] != '/'; i++ ) {
            days[j++] = dt[i];
        }
        j=0;
        
        for(i++; dt[i] != '/'; i++) {
            months[j++] = dt[i];
        }
        j=0;
        for(i++; i < dt.length(); i++) {
            years[j++] = dt[i];
        }
        this->year = atoi(years);
        this->month = atoi(months);
        this->day = atoi(days);
        //std::cout << this->toString() << "\n";
    }
    Date (int dy, int mn, int yr)
    {
       static int length[] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
       divided=0;
       month = max(1, mn);
       month = min(month,12);
       day = max(1,dy);
       day = min(day, length[month]);
       year = max(1, yr);
    }
    
    
    std::string toString()
    {
                std::string tmp="";
                std::stringstream conv;
                conv << GetDay() << "/" << GetMonth() << "/" << GetYear();
                tmp = conv.str();
    
                return tmp;
    }

private:
    int month, day, year;
}dt;


Date Date::operator/(int x)
{
      if(divided==2)
      {
                    this->divided++;
                    Date::SetYear(x);
      }
      
      else if(divided==1)
      {
           this->divided++;
           Date::SetMonth(x);
      }
     else if(divided==0)
      {
           this->divided++;
           Date::SetDay(x);
      }
    
    return *this;
}


void Date::SetDay(int day)
{
     this->day =day;
}

void Date::SetYear(int year)
{
     this->year=year;
}




/*
void Date::display()
{
   static char *name[] = {"nothing", "January", "February", "March", "April",
            "May", "June", "July", "August", "September", "October",
            "November", "December" };

}
*/

int Date::DaysSoFar()
{
   int total = 0;
   static int length[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

   for (int i=1; i < month; i++) total += length[i];
   total += day;
   return (total);
}

int Date::GetMonth()
{
   return month;
}

int Date::GetDay()
{
   return day;
}

int Date::GetYear()
{
   return year;
}



void Date::SetMonth(int mn)
{
   month = max(1, mn);
   month = min(month, 12);
}


class Time
{
public:
    int hours;
    int mins;
    Time() {}
    
    Time(char* time)//string format is "HH:MM"
    {
        char tmp[2];
        tmp[0]=time[0];
        tmp[1]=time[1];
        this->hours=atoi(tmp);
        //std::cout << this->hours;
        tmp[0]=time[3];
        tmp[1]=time[4];
        this->mins=atoi(tmp);
        //std::cout << this->mins << "\n";
    }
     Time(char* hours,char* mins)
    {
        this->hours=atoi(hours);
        this->mins=atoi(mins);
    }

     Time(int hours,int mins)
    {
        this->hours=hours;
        this->mins=mins;
    }
     int toMins(Time tmp)
     {
        int ttt=(tmp.hours)*60+tmp.mins;
        return ttt;
     }
     int toMins()
     {
         int  tmp=(this->hours)*60+this->mins;
         return tmp;
     }
     int minDifference(Time tmp)
     {
         return toMins(tmp)- this->toMins();
     }

     std::string toString()
     {
         
         std::stringstream conv;
         conv << this->hours << ":" << this->mins << "\n";
         return conv.str();
     }

};
