#include <list>
#include <map>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <set>
#include <functional>
#include <algorithm>
#include <typeinfo>
#include "Date.h"

#define HOURS   1
#define MINS    0
#define DOLLARS *1.2
#define EUROS   *1
#define SEARCH          db.initiateSearchList();
#define NEW_FLIGHT(x)   db.newFlight(toStr(x))?db.error()
#define FLIGHT(x,y)     db.flightCriteria(toStr(x),toStr(y));
#define AT(x)           db.atCriteria(toStr(x));
#define BUDGET(x,y)     db.budgetCriteria(x,1);
#define DURATION(x,y)   db.durationCriteria(x,y);
#define DIRECT          db.directCriteria();
#define INDIRECT(x)     db.indirectCriteria(x);
#define END             db.printSearchList()
#define ROUTE           newTmpRoute=Routes(0);db.tmp.routes.push_back(newTmpRoute);
#define FROM            newTmpRoute 
#define DATE            dt=Date()/
#define TO              arrival
#define toStr(x)	#x
#define TIME(x)         Time(toStr(x))
#define PRICE           db.tmp.budget=
using namespace std;

class pathClass {
      public:
      string city;
      Date pathDate;
      Time pathTime;
      
      public:
      pathClass() {}
      ~pathClass() {}
      
      string getCity() {
             return this->city;
      }
      
      string getDate() {
             return this->pathDate.toString();
      }
      
      void setCity(std::string _city) {
           this->city = _city;
      }
      
      void setDate(Date _date) {
           this->pathDate = _date ;
      }
      pathClass* operator[] (std::string args);
};

pathClass* pathClass::operator[] (std::string args)
{
           try {
               
               int i=0,j=0;
               char date[9];
               char time[5];
               Date dt;
               //cout << args;
               for(i; args[i] != '$'; i++ ) {
                      this->city += args[i];
               }
               i++;
               while( args[i] != '$' ) {
                      date[j++] = args[i];
                      i++;
               }
               date[j] = '\0';
               j = 0;
               for(i; i < args.length(); i++)
               {
                      time[j] = args[i];
                      j++;
               }
               time[j] = '\0';
               this->setDate( Date(date) );
               this->pathTime = Time(time);
               cout << this->getCity() << this->getDate() << this->pathTime.toString() << "\n";
               return this;
           } catch ( exception& e) {
                   cout << "Too few parameters given \n";
           }
}



class Routes{
public:
    
    pathClass departure;
    pathClass arrival;
    bool isFrom;
    
    Routes() {
             departure = pathClass();
             arrival = pathClass();
    }
    
    Routes* operator[] (std::string args);
    
    Routes(int _isFrom) {
                if(_isFrom == 0) {
                           this->isFrom = true;
                }
                else {
                     this->isFrom = false;
                }
    }
    
    int operator-(Routes other)
    {
        struct tm *start, *end;
        start->tm_year =  this->departure.pathDate.GetYear();
        start->tm_mon = this->departure.pathDate.GetMonth();
        start->tm_yday = this->departure.pathDate.DaysSoFar();
        start->tm_hour = this->departure.pathTime.hours;
        start->tm_min = this->departure.pathTime.mins;
        
        end->tm_year = other.arrival.pathDate.GetYear();
        end->tm_mon = other.arrival.pathDate.GetMonth();
        end->tm_yday = other.arrival.pathDate.DaysSoFar();
        end->tm_hour = other.arrival.pathTime.hours;
        end->tm_min = other.arrival.pathTime.mins;
     
        return difftime(mktime( end ), mktime( start ))/60;
    }
    
    
    
}newTmpRoute;


Routes* Routes::operator[] (std::string args)
{
     int i=0,j=0;
     char date[12];
     char time[6];
     string city;
     //cout << args;
     try {
         for(i; args[i] != '$'; i++ ) {
                city += args[i];
         }
         i++;
         while( args[i] != '$' ) {
                date[j++] = args[i];
                i++;
         }
         date[j] = '\0';
         j = 0;
 
         for(i; i < args.length(); i++)
         {
                time[j] = args[i];
                j++;
         }
         time[j] = '\0';
         this->departure.setCity(city);
         this->departure.setDate(Date(date));
         this->departure.pathTime = Time(time);
         cout << this->departure.getCity() << this->departure.getDate() << this->departure.pathTime.toString() << "\n";
         return this;
     } catch (exception& e) {
                 cout << "Too few parameters given! \n";
     }
}


class Flight
{
        public:
            string id;
            double budget;
            string departure;
            list <Routes> routes;
            Flight()
            {
            
            }
            Flight(string id)
            {
                this->id=id;
            
            }
            void setId(std::string _id)
            {
                id = _id;
            }
            
            void setBudget(double _budget)
            {
                budget = _budget;
            }
            
           
            
            
            string getId() {
                return id;
            }
            
            double getBudget()
            {
                return budget;
            }
            
            
            list <Routes> getRoutes() {
                return routes;
            }
            
            
            string getDeparture()
            {
                return this->getRoutes().front().departure.getCity();
            }
            
            string getArrival()
            {
                return this->getRoutes().back().arrival.getCity();
            }            
};








class Criteria
{
   public:
       bool Evaluate ( Flight& flight);

};

class BudCr:Criteria
{
public:
    double budget;
    BudCr(double budget,double type)
    {
             this->budget=budget*type;     
    }
   bool Evaluate( Flight& flight)
    {
        if(flight.budget<=this->budget)
        {
            return true;
        }
        else
        {
            return false;
        }
    }        
};


class DirCr:Criteria
{
  public:
   bool Evaluate( Flight& flight)
    {
        if(flight.routes.size()==1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }        
};

class IndirCr:Criteria
{
  public:
      int stops;
      IndirCr(int stops)
      {
          this->stops=stops;
      }
    bool Evaluate( Flight& flight)
    {
        if(flight.routes.size()>=stops)
        {
            return true;
        }
        else
        {
            return false;
        }
    }        
};

class DurCr:Criteria
{
public:
    int duration;
    DurCr(int ammount,int type)
    {
        if(type==0)
        {
            this->duration=ammount;
        }
        else
        {
            this->duration=ammount*60;
        }       
    }
    bool Evaluate( Flight& flight)
    {
        Routes temp, temp1;
        
        temp = flight.routes.front();
        temp1 = flight.routes.back();
        
        if( temp1 - temp < this->duration )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
};

class FlCr:Criteria
{
public:
    string departure;
    string arrival;
    FlCr(string departure,string arrival)
    {
            this->departure=departure;
            this->arrival=arrival;
               
    }
    bool    Evaluate( Flight& flight)
    {
        if(departure.compare(flight.getDeparture())==0
                &&arrival.compare(flight.getArrival())==0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
};

class AtCr:Criteria
{
  public:
      string departure;
      AtCr(string departure )
      {
          this->departure=departure;
      
      }
    bool  Evaluate( Flight& flight)
    {
        if(flight.departure.compare(departure))
        {
            return true;
        }
        else
        {
            return false;
        }
    }        
};



class DataBase
{
public:
            list < Flight > flights;
            list < Flight > Searchlist;
            Flight tmp;
            Routes newTmpRoute;
            
            void initiateSearchList(void)
            {
                for(list < Flight >::iterator i=flights.begin();i!=flights.end();++i)
                {
                    Searchlist.push_back(*i);
                }
            
            
            
            
            
            }
            
            
            void flightCriteria(string departure,string arrival)
            {
                for(list < Flight >::iterator i=Searchlist.begin();i!=Searchlist.end();++i)
                {
                    if(!FlCr(departure,arrival).Evaluate(*i))
                    {
                        Searchlist.erase(i);
                    }
                }
            }

            void directCriteria()
            {
                
                for(list < Flight >::iterator i=Searchlist.begin();i!=Searchlist.end();++i)
                {
                    if(!DirCr().Evaluate(*i))
                    {
                        Searchlist.erase(i);
                    }
                }

                
                
            }
            void indirectCriteria(int minstops)
            {
                
                for(list < Flight >::iterator i=Searchlist.begin();i!=Searchlist.end();++i)
                {
                    if(!IndirCr(minstops).Evaluate(*i))
                    {
                        Searchlist.erase(i);
                    }
                }

                
            }
            
            void budgetCriteria(double budget,int type)
            {
                double money;
                for(list < Flight >::iterator i=Searchlist.begin();i!=Searchlist.end();++i)
                {
                    if(!BudCr(money,type).Evaluate(*i))
                    {
                        Searchlist.erase(i);
                    }
                }

            }  
            
            void durationCriteria(int ammount,int type)
            {
                for(list < Flight >::iterator i=Searchlist.begin();i!=Searchlist.end();++i)
                {
                    if(!DurCr(ammount,type).Evaluate(*i))
                    {
                        Searchlist.erase(i);
                    }
                }
                
                
            }    

            void atCriteria(string departure)
            {
                
                for(list < Flight >::iterator i=Searchlist.begin();i!=Searchlist.end();++i)
                {
                    if(!AtCr(departure).Evaluate(*i))
                    {
                        Searchlist.erase(i);
                    }
                }
                
            }
            
            bool contains(list <std::string> cls, string str)
            {
                cout << " str = " << str << "\n";
            	list <string>::const_iterator it;
                
            	for(it = cls.begin(); it != cls.end(); ++it)
            	{
                    cout << " lol ";
                        if( it->compare( str ) == 0) return true;
            	}
            	return false;
            }
            
            void printSearchList()
            {
                /*
            	list < list<std::string>* >::const_iterator lii;
            	list <string> results;
            	
            	if(lis.size() == 1) {
            		return *lis.front();
            	}
            	
            	else {
            		lii = lis.begin();
            		list <string>::const_iterator sstr;
            		list<std::string> *tmp, *first = *lii;
            		for(sstr = first->begin(); sstr != first->end(); ++sstr)
            		{
            			lii = lis.begin();
            			++lii;
            			tmp = *lii;
            			while( lii != lis.end() && contains(*tmp, *sstr) ) {
            				++lii;
            				tmp = *lii;
            			}
            			if( lii == lis.end()  ) results.push_back( *sstr );
            		}
            		return results;
            	}*/
                 for(list < Flight >::iterator i=Searchlist.begin();i!=Searchlist.end();++i)
                {
                     cout<<" FlightId= "<<i->getId()<<"\n";
                }
            }
            
            bool error() {
                cout << "something went wrong in new flight register\n"; 
                return false;
            }
            
            bool newFlight(string id)
            {
                this->tmp=Flight(id);
                this->flights.push_back(this->tmp);
                return false;
            }
}db;


std::string  operator,(std::string str,Date tmp)
{
    //cout << tmp.toString() << "\n";
     return str+"$"+tmp.toString();

}

std::string operator,(std::string str, Time tmp)
{
    //cout << tmp.toString() << "\n";
    return str+"$"+tmp.toString();

}