/*
 *  main.cpp
 *  hy352
 *
 *  Created by Dimitrios on 01/02/2012.
 *  Copyright 2012 All rights reserved.
 *
 */


#include <iostream>
#include <list>
#include <iterator>


using namespace std;

bool contains(list <std::string>* cls, string str)
{
	list <string>::const_iterator it;
	for(it = cls->begin(); it != cls->end(); ++it)
	{
		if( it->compare( str ) == 0) return true;
	}
	return false;
}

list <string> intersection(list < list<std::string>* > lis)
{
	list < list<std::string>* >::const_iterator lii;
	list <string> results;
	
	if(lis.size() == 1) {
		return *lis.front();
	}
	
	else {
		lii = lis.begin();
		list <string>::const_iterator sstr;
		list<std::string> *tmp, *first = *lii;
		for(sstr = first->begin(); sstr != first->end(); ++sstr)
		{
			lii = lis.begin();
			++lii;
			tmp = *lii;
			while( lii != lis.end() && contains(tmp, *sstr) ) {
				++lii;
				tmp = *lii;
			}
			if( lii == lis.end()  ) results.push_back( *sstr );
		}
		return results;
	}

}

int main()
{
	list <string> t1, t2, t3,t4,t5;
	list < list<std::string>* > test;
	
	t1.push_back("front");
	t1.push_back("test3");
	t2.push_back("test");
	t2.push_back("test3");
	t3.push_back("test3");
	t3.push_back("end");
	t4.push_back("test3");
	t5.push_back("test3");
	
	list<string>::const_iterator it;
	
	test.push_back(&t1);
	test.push_back(&t2);
	test.push_back(&t3);
	test.push_back(&t4);
	test.push_back(&t5);
	
	list < list<std::string>* >::const_iterator lii;
	for(lii = test.begin(); lii != test.end(); ++lii)
	{
		list<std::string> *tmp = *lii;
		for(it = tmp->begin(); it!= tmp->end(); ++it)
		{
			cout << *it + "\n";
		}
	}
	list <string> kati = intersection(test);
	if ( kati.empty() ) return 0;
	else {
		list <string>::const_iterator it;
		cout << "Koina stoixeia: ";
		for(it = kati.begin(); it != kati.end(); ++it)
		{
			cout << *it+"\t";
		}
		cout << "\n";
	}
}
